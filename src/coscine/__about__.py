###############################################################################
# Coscine Python SDK
# Copyright (c) 2020-2023 RWTH Aachen University
# Licensed under the terms of the MIT License
# For more information on Coscine visit https://www.coscine.de/.
###############################################################################

"""
Coscine Python SDK package metadata
"""

__author__ = "RWTH Aachen University"
__copyright__ = "RWTH Aachen University"
__license__ = "MIT License"
__version__ = "0.11.10"
