boto3 # Amazons S3 SDK
isodate # Parse datetime strings
tqdm # CLI progress bars
pyshacl # RDF SHACL shapes constraints verification
rdflib # Deals with RDF
requests # Deals with HTTP
requests-cache # Caches HTTP requests
requests-toolbelt # Multipart upload monitoring
tabulate # Pretty prints data in tabular format
sphinx # documentation generator
furo # sphinx documentation theme
myst-parser # sphinx plugin for markdown support
sphinx-copybutton # inserts buttons to copy sourcecode in docs
setuptools # for installation and packaging (conda doesn't have it)
