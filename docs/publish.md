# Publishing data
As a researcher you probably want to publish your research data at some point
in time. Coscine provides you with a data management platform, but only
you and your project members can access the data.  
To facilitate data sharing and promote the FAIR principles,
the Coscine Python SDK enables you to provide your data to external
users without requiring them to register and join your project.

## The technical details
For each file that is stored inside of a Coscine resource, a download url
is generated. This download url is valid for a limited timeframe and expires
after this timeframe has been exceeded. Using the Coscine Python SDK one
can fetch the download url and expiry timestamp for each file of interest
and make them available on a 3rd party platform.

```python
import json
import coscine
client = coscine.ApiClient(TOKEN)
index = client.project("My Project").resource("My Resource").file_index()
with open("index.json", "w", encoding="utf-8") as fp:
    json.dump(index, fp)
```

As 3rd party platforms numerous contenders come in mind:  
* [GitHub]
* [GitLab]
* [Google Drive]
* [Sciebo]
* ...

Simply drop your file index on one of these websites and share the link
with peers or fetch the file via software if you use your data in
a software package. Note that the file must be refreshed every X hours
(usually 24 hours) in order to keep the download urls from expiring.  
[GitHub] and [GitLab] present themselves as prime candidates
to automate refreshing the files via the integration of GitHub Actions
and GitLab Actions.

## Even fancier: A website for your research data
Talking about [GitHub] and [GitLab] and their ability to automate workflows
and host files brings up another topic - [GitHub Pages] and [GitLab Pages].
Both [GitHub] and [GitLab] enable you to host static websites for free and
under your own domain. With this in mind we can even generate a website
for our research data and share that website with our peers. This allows
us to present our data in a friendlier way and obviously it is a lot more
pretty and offers many more opportunities of presenting our data.  
For more information on how one would accomplish this take a look
at the Coscine community project called [Coscine Static Site Generator].


[GitHub]: https://github.com/
[GitHub Pages]: https://pages.github.com/
[GitLab]: https://gitlab.com/explore/projects
[GitLab Pages]: https://docs.gitlab.com/ee/user/project/pages/
[Google Drive]: https://drive.google.com/
[Sciebo]: https://www.hochschulcloud.nrw/
[Coscine Static Site Generator]: https://git.rwth-aachen.de/coscine/community-features/coscine-static-site-generator
