# Installation
### Installing Python 🐍
Most users do not have Python installed by default, so we begin with the
installation of Python itself. To check if you already have Python installed,
open a Command Prompt (on Windows: Win+r and type cmd).
Once the command prompt is open, type python --version and press Enter.
If Python is installed, you will see the version of Python printed to the
screen. If you do not have Python installed, refer to the installation guides
in the [Hitchhikers Guide to Python]. You must install Python 3. 
Make sure that you choose an [appropriate version] for installation!  
Once Python is installed, you can install the Coscine Python SDK using pip.  
Other useful links on Python:
- [https://wiki.python.org/moin/BeginnersGuide](https://wiki.python.org/moin/BeginnersGuide)
- [https://www.python.org/downloads/](https://www.python.org/downloads/)

In the following snippets, depending on your installation and platform, you may have to substitute `python` with `py` or `python3`.

### Installing the Coscine Python SDK
#### Via the Python Package Index
The Coscine Python SDK package is hosted on the Python Package Index (PyPi).
You can install and update it and all of its dependencies via the Python
Package Manager (pip):  
```bash
python -m pip install --upgrade coscine
```
The `--upgrade` option updates the package if it is already installed
on your system.

#### Via Conda
This module's pypi version is mirrored in conda forge. You can install it
and all of its dependencies via Conda:  
```bash
conda install -c conda-forge coscine
```
and likewise for updating it you would do:  
```bash
conda update -c conda-forge coscine
```

#### Via Git
You can install the python module with a local copy of the source code,
which you can grab directly from this repository via git:  
```bash
git clone https://git.rwth-aachen.de/coscine/community-features/coscine-python-sdk.git
cd ./coscine-python-sdk
py setup.py
```
The `master` branch should contain the latest stable release version.
If you want to make sure, refer to one of the git tags.  
Development is done in the `dev` branch - the most recent and not always
100% stable versions can be found there. It is recommended that you stick
to the master branch for installations.

[Hitchhikers guide to python]: https://docs.python-guide.org/
[appropriate version]: https://devguide.python.org/versions/
