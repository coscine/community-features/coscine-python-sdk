# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

from coscine.__about__ import __author__, __version__

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'coscine'
copyright = '2023, RWTH Aachen University'
author = __author__
release = __version__

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    'sphinx.ext.autodoc',  # extracts python docstrings
    'sphinx.ext.napoleon', # ability to use numpy-style docstrings
    'sphinx.ext.viewcode', # adds a link to the sourcecode in docs
    'sphinx.ext.mathjax',  # math formula rendering
    'sphinx_copybutton',   # inserts buttons to copy sourcecode
    'myst_parser'          # ability to use markdown in place of .rst
]

templates_path = ['_templates']
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']



# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'furo'
html_static_path = ['_static']

# based on html_static_path as the root directory
html_theme_options = {
    "light_logo": "coscine_logo_rgb.png",
    "dark_logo": "coscine_logo_white_rgb.png",
    "source_edit_link": (
        "https://git.rwth-aachen.de/coscine/community-features"
        "/coscine-python-sdk/-/tree/master/docs"
    )
}
html_favicon = "_static/coscine_python_sdk_icon_rgb.png"
