# Creating an API token 🔐
You need an API token to use the Coscine API. If you have
not already, [create a new API token].
Once you have an API token you are ready to use the API.

### A word of advice ⚠
The token represents sensible data and grants anyone in possesion of it full
access to your data in coscine. Do not leak it publicly on online platforms!
Do not include it within your sourcecode if you intend on uploading it
to the internet or sharing it among peers. Take precautions
and follow best practices to avoid corruption, theft or loss of data!

### Using the API token
There are two simple and safe methods of using the API token without exposing
it to unintended audiences.

#### Loading the token from a file
Simply put your API token in a file on your harddrive and read the file when
initializing the Coscine client. This has the advantage of keeping the token
out of the sourcecode and offering the user an easy way to switch between
tokens by changing the filename in the sourcecode.  
```python
with open("token.txt", "rt") as fp:
	token = fp.read()
# You can now use token to intialize the coscine ApiClient!
```
This approach comes at the disadvantage of potentially exposing the token by
accidentially leaking the file together with the sourcecode e. g. on online
platforms, especially sourcecode hosting platforms such as GitHub or GitLab.
Therefore precautions must be taken. When using git as a versioning system,
a `.gitignore` file that inclues any possible token name or file extension
should be mandatory. This allows you to exclude any filename like `token.txt`.
An even better way would be to agree upon a common token file extension
such as `.token` and exclude that file extension. By doing this you can
safely upload/push your code to the usual suspects, since the `.gitignore`
makes sure your sensitive files are not included in any of these uploads.
- [gitignore guide by W3schools.com]
- [gitignore docs on git-scm.com]

#### Keeping the token in an environment variable
This method does not rely on any files but instead on environment variables.
Simply set an environment variable containing your token and use that
variable from your python program.  
**Set environment variable:**  
```python
import os
os.environ["COSCINE_API_TOKEN"] = "My Token Value"
```
You would do this only once, and obviously not in your final program/script.
There are alternative platform-specific methods for creating an environment
variable (see links given below), but this pythonic approach should work on
most platforms out of the box. Just don't do this in your actual program!
Do it via a dedicated script or in an interactive python session (shell).  
**To get the environment variable:**  
```python
import os
token = os.getenv("COSCINE_API_TOKEN")
```
This is certainly a little more complex for some users, especially users who
may want to use your program. They can easily share tokens by sending
a file to colleagues but sharing environment variables requires each user
to additionally create the environment variable on their local system.  

Find out how to temporarily or permanently set environment variables
on certain Operating Systems:  
- [Windows]
- [Linux]
- [Apple]

[create a new API token]: https://docs.coscine.de/en/token
[gitignore guide by W3schools.com]: https://www.w3schools.com/git/git_ignore.asp
[gitignore docs on git-scm.com]: https://git-scm.com/docs/gitignore
[Windows]: http://www.dowdandassociates.com/blog/content/howto-set-an-environment-variable-in-windows-command-line-and-registry/
[Linux]: https://phoenixnap.com/kb/linux-set-environment-variable
[Apple]: https://i.kym-cdn.com/photos/images/newsfeed/001/765/157/333.jpg
