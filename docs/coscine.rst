coscine package
===============

Submodules
----------

coscine.client module
---------------------

.. automodule:: coscine.client
   :members:
   :undoc-members:
   :show-inheritance:

coscine.common module
---------------------

.. automodule:: coscine.common
   :members:
   :undoc-members:
   :show-inheritance:

coscine.exceptions module
-------------------------

.. automodule:: coscine.exceptions
   :members:
   :undoc-members:
   :show-inheritance:

coscine.metadata module
-----------------------

.. automodule:: coscine.metadata
   :members:
   :undoc-members:
   :show-inheritance:

coscine.project module
----------------------

.. automodule:: coscine.project
   :members:
   :undoc-members:
   :show-inheritance:

coscine.resource module
-----------------------

.. automodule:: coscine.resource
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: coscine
   :members:
   :undoc-members:
   :show-inheritance:
