# Interacting with Projects
Coscine is a project-based data management platform. A project represents
the central hub for all of the data resources and people involved
in a scientific undertaking.

## The Project object
The Coscine Python SDK abstracts Coscine projects with a
custom Project datatype. An instance of type `coscine.Project`
has the following properties:  
| Property                 | Datatype | Modifiable  | Description |
|--------------------------|----------|-------------|-------------|
| created                  |  date    | ❌ | Timestamp when the project was created |
| creator                  |  str     | ❌ | Coscine user ID of the project creator |
| description              |  str     | ✔  | Project description                    |
| disciplines              | list[Discipline] | ✔ | List of scientific disciplines  |
| display_name             |  str     | ✔  | Short project name as displayed in the web interface |
| end_date                 | date     | ✔  | Project end date         |
| grant_id                 | str      | ✔  | Project grant ID         |
| id                       | str      |  ❌ | Coscine Project ID       |
| keywords                 | list[str] | ✔ | List of related keywords |
| name                     | str     | ✔ | Full project name |
| organizations            | list[Organization] | ✔ | List of involved organizations |
| pid                      | str    | ❌ | Persistent Identifier for the project |
| principal_investigators  | str    | ✔ | Project investigators |
| slug                     | str    | ❌ | Coscine project slug |
| start_date               | date   | ✔ | Project start date |
| url                      | str    | ❌ | Project URL as in the web browser |
| visibility               | Visibility | ✔ | Project visibility setting |

Properties can be accessed via the usual dot notation
i. e. `print(project.name)`.
Certain properties can be modified (✔) locally and updated remotely
by calling `Project.update()`.

## Accessing projects
The following snippet demonstrates how to get a list of
Coscine Projects and prints all returned projects by their
full name. By default, the call `client.projects()` will only return
toplevel projects (i.e. only the projects at the root level).
To include subprojects in the results, the *toplevel*
parameter should be set to `False`.
```python
import coscine

client = coscine.ApiClient("My Coscine API Token")
projects = client.projects()
for project in projects:
	print(project.name)
```

The list of projects can be filtered according to certain unique project
properties. In the subsequent snippet, only one project is returned, saving
us from manually going through the list if we want to access a specific project.
```python
try:
	project = client.project("My Project Display Name")
	print(project.name)
except coscine.TooManyResults:
	print("Found more than 1 project with the same property!")
except coscine.NotFoundError:
    print("Failed to find a project via the property!")
```

We can filter projects according to these properties:  
* client.project("Display Name", Project.display_name)
* client.project("Full Name", Project.name)
* client.project("Project ID", Project.id)
* client.project("Persistent Identifier", Project.pid)
* client.project("Web URL", Project.url)
The defaulft property is the display name, which is the 25 character shortend version of the project name.
Other properties are rejected!

## Creating a new project
Projects can be created via a simple function call. The new project
object is returned immediately after the project has been created.

```python
import coscine
from datetime import datetime
client = coscine.ApiClient("My Coscine API Token")
project = client.create_project(
    "Full Name",
    "Display Name",
    "Project Description",
    datetime.now().date(),
    datetime.now().date(),
    "Principal Investigator",
    [client.discipline("Computer Science 409")],
    [client.organization("https://ror.org/04xfq0f34")],
    # -> RWTH Aachen University
    client.visibility("Project Members")
)
print(project)
```

## Deleting a project
If we create projects with the Coscine Python SDK we may also need to
delete some projects.

```python
import coscine
client = coscine.ApiClient("My Coscine API Token")
project = client.project("Some Project I don't like")
try:
    project.delete()
except coscine.CoscineException:
    print("Something went wrong. Maybe we are not authorized to delete that project. :(")
```
Be aware that this function may fail due to unsufficient privileges.
Only project owners can delete a project - members can not!
After calling `delete()` we should also not use the project object in python anymore,
as it is invalid in Coscine (but can still be interacted with locally).

## Downloading a project
Downloading a project and all of the resources inside of that project
is easy with the `Project.download()` method. It accepts an optional
path where it should save the project to. If no path is specified, the
current local directory is chosen as path.

```python
import coscine

client = coscine.ApiClient("My Coscine API Token")
project = client.project("Fancy project - please don't steal")
project.download(path="./")  # This is my project now hehehe
```

## Member management
Coscine Projects contain Project Members. We can get a list of the members
of a project by calling the `Project.members()` method.

```python
import coscine

client = coscine.ApiClient("My Coscine API Token")
project = client.project("My Project with Members")
members = project.members()
for member in members:
    print(member.name)
    print(member.email)

# Set project role for a member
member.role = client.role("Owner")
    
# Remove a member from the project
project.remove_member(member)
```

We can add Coscine users to our project:  
```python
import coscine

client = coscine.ApiClient("My Coscine API Token")
project = client.project("My Project with Members")
for user in client.users("*@institute.rwth-aachen.de"):
    project.add_member(user, client.role("Member"))
```

Similarly we can invite new members via their E-Mail.  
```python
project.invite("john@example.com", "Member") # Or Owner / Guest
```
