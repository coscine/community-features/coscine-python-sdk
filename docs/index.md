---
hide-toc: true
---

# Coscine Python SDK Docs 📚
You use the Coscine Python SDK to interact with the Coscine REST API
from Python. The SDK provides a user-oriented high-level and
object-oriented API as well as low-level methods for easily extending
the functionality and implementing custom routines.  
It is aimed at developers of all levels of experience - this specifically
includes those who may not have comprehensive programming expertise
and just want to "get stuff done" (i.e. the vast majority
of people reading this).
Therefore this documentation provides copy-pastable examples
specifically tailored for those kinds of people as well as in-depth
guides for those, who would like to delve deeper into implementation details.

## Recommendations 🤓
*Vim, emacs and nano users may want to scroll past this paragraph...*  
It is recommended to use an advanced text editor or integrated development
environment (IDE) for writing programs with the Coscine Python SDK.
Tools like [Visual Studio Code] make use of the docstring annotations
inside of this packages sourcecode and display a detailed description
for most of the classes and functions exposed by this package directly
in the editor. Obviously you could also use a simple text editor like
notepad, but your experience will be severely impacted by your choice of tool.

## Feedback 🙋
If you stumble upon an error and believe it is related to
the Coscine Python SDK you are more than welcome to report it.
Active collaboration, bug reports and feature requests are very welcome.
To report a bug or to request a feature simply open an issue on
the projects [issue tracker].  
To contribute features that you have developed yourself and believe
to benefit everyone if integrated into the SDK you may also open a
new merge request.

[issue tracker]: https://git.rwth-aachen.de/coscine/community-features/coscine-python-sdk/-/issues
[Visual Studio Code]: https://code.visualstudio.com/

```{toctree}
:hidden:

installation
api-token
client
project
resource
metadata
publish
API Reference<genindex>
modindex
build
```
